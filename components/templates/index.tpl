<!DOCTYPE html>
<html lang="en">
<head>
    [[$head]]
</head>
<body>
    <header class="header">
    <div class="wrapper-outer">
        <div class="header__wrapper">

            <div class="header__logo-title">
                <img src="[[*header__logo]]" alt="logo" class="header__logo">
                <h1 class="header__title">[[*header__title]]</h1>
            </div>

            <div class="header__mail-tel">
                <div class="contact__block">
                    [[getImageList? 
                    &tvname=`email`
                    &tpl=`@CODE:
                        <a class="contact__link" href="mailto:[[+email]]"><img src="[[+email__icon]]" alt="icon mail" class="contact__icon">
                            [[+email]]
                        </a>
                    `
                    ]]
                </div>
                <div class="contact__block_not-tablet">
                    [[getImageList? 
                    &tvname=`phone`
                    &tpl=`@CODE:
                        <a class="contact__link" href="tel:[[+phone_right]]"><img src="[[+phone__icon]]" alt="icon phone" class="contact__icon">
                            [[+phone]]
                        </a>
                    `
                    ]]
                </div>

            </div>

            <div class="contact__block contact__block_tablet">
                [[getImageList? 
                &tvname=`phone`
                &tpl=`@CODE:
                <a class="contact__link contact__link_tablet" href="tel:[[+phone_right]]"><img src="[[+phone__icon]]" alt="icon phone" class="contact__icon">
                </a>
                `
                ]]
            </div>
            
            <div class="contact__block">
                [[getImageList? 
                &tvname=`map`
                &tpl=`@CODE:
                <a class="contact__link" href="[[+map__link]]" target="_blank">
                    <img src="[[+map__icon]]" alt="icon marker" class="contact__icon">
                    [[+map__text]]
                </a>
                `
                ]]
            </div>

            <!-- hamburger -->

            <div class="hamburger">
                <span class="hamburger__line"></span>
                <span class="hamburger__line hamburger__line_center"></span>
                <span class="hamburger__line"></span>
            </div>

            <!-- / hamburger -->

            <button class="header__button">
                <a class="button__link" href="#contacts">[[*contact__button-text]]</a> 
            </button>

        </div>
    </div>
</header>
    
    <main class="main">
        <section class="hero">
    <div class="wrapper-outer">
        <div class="hero__dots-left">
            <img class="hero__dots-left_pos" src="../assets/img/icons/dots_3x6_vertical.svg" alt="decoration dots">
        </div>

        <div class="wrapper-inner">

            <div class="hero__content">
                <h2 class="hero__title">[[*hero__title]]</h2>
                <button class="hero__button">
                    <a class="button__link" href="#contacts">[[*contact__button-text]]</a> 
                </button>
            </div>

        </div>

        <div class="hero__background">
        </div>
        <img class="hero__dots-right" src="../assets/img/icons/dots_hero_right.svg" alt="decoration dots">
        
    </div>

    

</section>

        <section class="offer">
    <div class="wrapper-inner wrapper-wide">

        <div class="offer__header">
            <h2 class="offer__title">[[*offer__title]]</h2>
            <div class="offer__controls">
                <div class="swiper-btn-prev"> <img class="swiper__arrow-left" src="../assets/img/icons/arrow-left.svg" alt="prev slide"> </div>
                
                <div class="swiper-btn-next"> <img class="swiper__arrow-right" src="../assets/img/icons/arrow-left.svg" alt="next slide"> </div>
            </div>
        </div>

        <div class="offer__content">
            <div class="offer__info">
                <p class="offer__price">[[*offer__price]]</p>

                <p class="offer__text">[[*offer__text]]</p>

                <div class="offer__button">
                    <a class="button__link" href="#contacts">[[*contact__button-text]]</a> 
                </div>
            </div>

            <div class="swiper-container-m">
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->

                        [[getImageList?
                        &tvname=`offer__slides`
                        &tpl=`@CODE:
                        <div class="swiper-slide">
                            <img src="[[+offer__slide-image]]" alt="slider image" class="swiper-image">
                        </div>
                        `
                        ]]

                    </div>

                </div>

                <div class="swiper-btn-next swiper-btn-next_m"> <img class="swiper__arrow-right swiper__arrow-right_m"
                        src="../assets/img/icons/arrow-right_m.svg" alt="next slide"> </div>
            </div>
            

            <div class="offer__button offer__button_m">
                <a class="button__link" href="#contacts">[[*contact__button-text]]</a> 
            </div>
           
        </div>
    </div>
</section>

        <section class="about">
    <div class="wrapper-outer">
        <div class="about__dots-left">
            <div class="about__bg-left"></div>
            <img src="../assets/img/icons/dots_3x6_vertical.svg" alt="decoration dots" class="about__dots">
        </div>
    
        <div class="wrapper-inner">
            <div class="about__content">
                <div class="about__left-side">
                    <img src="[[*about__image]]" alt="photo" class="about__image">
                </div>
    
                <div class="about__right-side">
                    <h2 class="about__title">[[*about__title]]</h2>
    
                    <p class="about__text">[[*about__text]] </p>
                </div>
            </div>
        </div>
    </div>

</section>

        <section class="contacts" id="contacts">
    <div class="wrapper-outer">
        <div class="contacts__dots">
            <img src="../assets/img/icons/contacts__dots_up.svg" alt="" class="contacts__dots-up">
            <img src="../assets/img/icons/dots_6x3_gorizontal.svg" alt="" class="contacts__dots-down">
        </div>

        <div class="wrapper-inner">
            <h2 class="contacts__title">[[*contacts__title]]</h2>

            <div class="contacts__content">

                [[!AjaxForm?
                &snippet=`FormIt`
                &form=`contact__form`
                &hooks=`email`
                &emailSubject=`Тестовое сообщение`
                &emailTo=`goodbish@mail.ru`
                &emailFrom=`growtask_test@post.et9.ru`
                &validate=`email:email:required`
                &validationErrorMessage=`В форме содержатся ошибки!`
                &successMessage=`Сообщение успешно отправлено`
                ]]

                <div class="contacts__contacts">
                    <p class="contacts__contacts-title">[[*contacts__contacts-title]]</p>

                    <div class="contact__block contact__block_visible">
                        [[getImageList? 
                        &tvname=`email`
                        &tpl=`@CODE:
                            <a class="contact__link contact__link_visible" href="mailto:[[+email]]"><img src="[[+email__icon]]" alt="icon mail" class="contact__icon contact__icon_sm">
                                [[+email]]
                            </a>
                        `
                        ]]
                    </div>

                    <div class="contact__block contact__block_visible">
                        [[getImageList? 
                        &tvname=`phone`
                        &tpl=`@CODE:
                            <a class="contact__link contact__link_visible" href="tel:[[+phone_right]]"><img src="[[+phone__icon]]" alt="icon phone" class="contact__icon contact__icon_sm">
                                [[+phone]]
                            </a>
                        `
                        ]]
                    </div>

                    <div class="contact__block contact__block_visible">
                        [[getImageList? 
                        &tvname=`map`
                        &tpl=`@CODE:
                        <a class="contact__link contact__link_visible" href="[[+map__link]]">
                            <img src="[[+map__icon]]" alt="icon marker" class="contact__icon contact__icon_sm">
                            [[+map__text]]
                        </a>
                        `
                        ]]
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    </main>

    <footer class="footer">
    <div class="wrapper-outer">
        <div class="footer__wrapper">
            <div class="footer__left">
                <a href="#" class="footer__text">[[*footer__title]]</a>
            </div>
    
            <div class="footer__right">
                <a href="[[++site_url]]privacy" class="footer__text" target="_blank">Политика конфиденциальности</a>
                <span class="divider">|</span>
                <a href="[[++site_url]]offer.html" class="footer__text" target="_blank">Публичная Оферта</a>
            </div>
        </div>
    </div>
</footer> 
    <script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>

    <script src="assets/js/script.min.js"></script>

</body>
</html>