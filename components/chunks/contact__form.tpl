<form method="POST" class="contacts__form">
    <p class="contacts__form-title">[[*form__title]]</p>

    <input type="text" name="name" id="name" class="contacts__input" placeholder="Ваше имя">

    <input type="tel" name="tel" id="tel" class="contacts__input" placeholder="Ваш номер телефона">

    <input type="email" name="email" id="email" class="contacts__input" placeholder="Ваш e-mail">

    <button class="contacts__button form__button" type="submit">[[*form__button-text]]</button>

</form>